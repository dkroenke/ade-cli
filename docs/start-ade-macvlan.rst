.. _start-ade-macvlan:

Starting ADE with macvlan network configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In situations where ADE needs to communicate with external resources
(e.g., sensors), it is possible to use a `Macvlan network`_ to assign the
ADE container a MAC address, such that the container appears to be
physically connected to the network interface. Macvlan gives the advantage
of allowing the container to be isolated from the host while appearing
as a physical device on the network.

To use Macvlan, follow the offical `Docker documentation`_ to create a
``docker network`` (e.g. ``pub_net``), then run ``ade start`` with an extra Docker arguments: e.g

1. On the host create a macvlan network::

   $ docker network create -d macvlan --subnet 192.168.1.0/24 --gateway 192.168.1.1 -o parent=eno1 pub_net


   **Note**
      The subnet and gatewar must match the local network settings

2. Start ``ade`` with ``pub_net`` and an unused IP address::

   $ ade start -- --net=pub_net --ip=192.168.1.101


3. To avoid typing the arguments every time, add the option to ``ADE_DOCKER_RUN_ARGS`` in
the ``.aderc``: e.g,

.. literalinclude:: example-aderc
   :language: bash


.. _Macvlan network: https://docs.docker.com/network/macvlan/
.. _Docker documentation: https://docs.docker.com/network/macvlan/#create-a-macvlan-network
